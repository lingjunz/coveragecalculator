
import os
import time
import sys
import numpy as np
sys.path.append("../")
os.environ['CUDA_DEVICE_ORDER'] = 'PCI_BUS_ID'
os.environ['CUDA_VISIBLE_DEVICES'] = '0'#'0,1,2,3'
from collections import defaultdict

import  torch
from  torch import nn
import torch.backends.cudnn as cudnn
import torchvision.transforms as trn
import torchvision.datasets as dset
from torch.utils.data import DataLoader

from utils.MyDataset import UserDataset
from utils.helper_function import make_dataloader
from train_utils import train_baseline,train_oe,test,adjust_opt
from train_utils import select_model_optimizer,check_dir
from utils.configs import mnist_transform
import keras
from keras.datasets import mnist,cifar10
keras.backend.set_image_data_format("channels_last")
import joblib



def train_model(selection = "lenet5",oe_tuned = False,dataset="mnist", num_classes = 10,dropRate=0.0):
    # Configuration
    cudnn.benchmark = True  
    state = defaultdict()
    start_epoch = 0
    end_epoch = 100
    opt = "sgd"
    save_path = "../pretrained/{}_pytorch_{}_{}".format(dataset,selection,end_epoch)
    model_name = "{}_{}".format(selection, "oe" if oe_tuned else "baseline")
    if dataset=="fmnist":
        train_data = dset.FashionMNIST('/home/lingjun/ood_2020/data/pytorch_data/fashionMnist', train=True, transform=mnist_transform)
        test_data = dset.FashionMNIST('/home/lingjun/ood_2020/data/pytorch_data/fashionMnist', train=False, transform=mnist_transform)
    elif dataset=="mnist":
        train_data = dset.MNIST('/home/lingjun/ood_2020/data/pytorch_data/mnist', train=True, transform=mnist_transform)
        test_data = dset.MNIST('/home/lingjun/ood_2020/data/pytorch_data/mnist', train=False, transform=mnist_transform)
    elif dataset=="mnist_adv":
        (x_train,y_train),(x_test,y_test) = mnist.load_data()
        y_train,y_test = np.int64(y_train.squeeze()),np.int64(y_test.squeeze())
        
        adv_samples = np.uint8(np.load("../train_seeds/mnist/{}/PGD_adv_samples.npy".format(selection))*255)
        adv_labels = np.load("../train_seeds/mnist/{}/PGD_adv_labels.npy".format(selection))
        
        all_train = np.concatenate((np.expand_dims(x_train,-1),adv_samples),axis=0)
        all_labels = np.int64(np.concatenate((y_train,adv_labels),axis=0))
        print(">>>>>>>Train data:{},adv_samples:{},adv_labels:{}".format(all_train.shape,adv_samples.shape,adv_labels.shape))

        train_data = UserDataset(all_train,labels = all_labels,img_size = (28,28), transform=mnist_transform)
        test_data = UserDataset(x_test,labels = y_test,img_size = (28,28), transform=mnist_transform)

    else:
        assert False,"undefined dataset:{}".format(dataset)
    train_loader = torch.utils.data.DataLoader(train_data, batch_size=256, shuffle=True,num_workers=4, pin_memory=True)
    test_loader = torch.utils.data.DataLoader(test_data, batch_size=256, shuffle=False,num_workers=4, pin_memory=True)
    
    if oe_tuned:
        ood_transform = trn.Compose([ trn.RandomHorizontalFlip(),trn.ToTensor()])
        ood_path = "/home/lingjun/ood_2020/data/training_ood_data/kmnist_20000.npy"
        ood_samples = np.load(ood_path)
        train_loader_out = make_dataloader(ood_samples,None,img_size=(28,28),batch_size=40,transform_test=ood_transform)
        train_loader_in = train_loader

    # Load model structure
    net,optimizer = select_model_optimizer(selection,opt,num_classes,dropRate)
    net.cuda()
    # Make save directory
    check_dir(save_path)

    with open(os.path.join(save_path,  '{}_training_results.csv'.format(model_name)), 'w') as f:
        f.write('epoch,time(s),train_loss,train_acc,test_loss,test_acc(%)\n')
    print('Beginning Training\n')
    # Main loop
    best_epoch = 0
    best_acc = 0.0
        
    for epoch in range(start_epoch, end_epoch):
        
        adjust_opt(opt, optimizer, epoch)
        if epoch % 50 == 0:
            print(">>>>LR:{}".format(optimizer.param_groups[0]['lr']))
        state['epoch'] = epoch
        begin_epoch = time.time()
        if oe_tuned:
            train_oe(net,train_loader_in,train_loader_out,optimizer,state)
        else:
            train_baseline(net,train_loader,optimizer,state)
        test(net,test_loader,state)
         # Save model
        if epoch==0:
            best_epoch = epoch
            best_acc = state['test_accuracy']
            cur_save_path = os.path.join(save_path, '{}_epoch_{}_{}.pt'.format(model_name,best_epoch,best_acc))
            torch.save(net.state_dict(),cur_save_path)
        cur_acc = state['test_accuracy']
        if cur_acc > best_acc:
            cur_save_path = os.path.join(save_path, '{}_epoch_{}_{}.pt'.format(model_name,epoch,cur_acc))
            torch.save(net.state_dict(),cur_save_path)
            prev_path = os.path.join(save_path, '{}_epoch_{}_{}.pt'.format(model_name,best_epoch,best_acc))
            if os.path.exists(prev_path): 
                os.remove(prev_path)
            best_epoch = epoch
            best_acc = cur_acc
        # Show results    
        with open(os.path.join(save_path,  '{}_training_results.csv'.format(model_name)), 'a') as f:
            f.write('%03d,%05d,%0.6f,%0.4f,%0.6f,%0.4f\n' % (
                (epoch + 1),
                time.time() - begin_epoch,
                state['train_loss'],
                state['train_accuracy'],
                state['test_loss'],
                state['test_accuracy'],
            ))
        print('|Epoch {0:3d} | Time {1:5d} | Train Loss {2:.4f}|  Train Acc {3:.4f} | Test Loss {4:.4f} | Test Acc {5:.4f}'.format(
        (epoch + 1),
        int(time.time() - begin_epoch),
        state['train_loss'],
        state['train_accuracy'],
        state['test_loss'],
        state['test_accuracy'])
        )
    
if __name__=="__main__":

#     train_model(selection = "lenet1",oe_tuned = False, dataset="mnist_adv", num_classes = 10)
    
    train_model(selection = "ConvnetMnistBN",oe_tuned = False, dataset="mnist", num_classes = 10)

    
