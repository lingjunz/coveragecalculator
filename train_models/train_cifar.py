
import os
import sys
import time
import numpy as np
from collections import defaultdict
sys.path.append("../")
# os.environ['CUDA_DEVICE_ORDER'] = 'PCI_BUS_ID'
# os.environ['CUDA_VISIBLE_DEVICES'] = '1'#'0,1,2,3'

import  torch
from  torch import nn
import torch.backends.cudnn as cudnn
import torchvision.datasets as dset
from torch.utils.data import DataLoader


from utils.MyDataset import UserDataset
from utils.helper_function import make_dataloader

from utils.configs import cifar10_transforms_test,cifar10_transforms_train,cifar100_transforms_train,cifar100_transforms_test,svhn_transforms_train,svhn_transforms_test
from train_utils import select_model_optimizer,check_dir
from train_utils import train_baseline,train_oe,test,adjust_opt
from keras.datasets import cifar10


def train_model(selection = "resnet18",oe_tuned = False, dataset="cifar10",num_classes = 10,dropRate=0.0):

    # Configuration
    cudnn.benchmark = True  
    state = defaultdict()
    start_epoch = 0
    end_epoch = 200
    opt = "sgd"

    save_path = "../pretrained/{}_pytorch_{}_{}".format(dataset,selection,end_epoch)
    model_name = "{}_{}".format(selection, "oe" if oe_tuned else "baseline")
    
    if dataset=="cifar10":
        train_data = dset.CIFAR10('/home/lingjun/ood_2020/data/pytorch_data/cifar10', train=True, transform=cifar10_transforms_train)
        test_data = dset.CIFAR10('/home/lingjun/ood_2020/data/pytorch_data/cifar10', train=False, transform=cifar10_transforms_test)
    elif dataset=="cifar100":
        train_data = dset.CIFAR100('/home/lingjun/ood_2020/data/pytorch_data/cifar100', train=True, transform=cifar100_transforms_train)
        test_data = dset.CIFAR100('/home/lingjun/ood_2020/data/pytorch_data/cifar100', train=False, transform=cifar100_transforms_test)
    elif dataset=="svhn":
        x_train = np.load("../svhn/svhn_train_x.npy")
        y_train = np.int64(np.load("../svhn/svhn_train_y.npy").squeeze()-1)

        x_test = np.load("../svhn/svhn_test_x.npy")
        y_test = np.int64(np.load("../svhn/svhn_test_y.npy").squeeze()-1)

        train_data = UserDataset(x_train,labels = y_train,img_size = (32,32), transform=svhn_transforms_train)
        test_data = UserDataset(x_test,labels = y_test,img_size = (32,32), transform=svhn_transforms_test)
        

    elif dataset=="cifar10_adv":
        (x_train,y_train),(x_test,y_test) = cifar10.load_data()
        y_train,y_test = np.int64(y_train.squeeze()),np.int64(y_test.squeeze())
        
        adv_samples = np.uint8(np.load("../cifar10/{}/PGD_adv_samples.npy".format(selection))*255)
        adv_labels = np.load("../cifar10/{}/PGD_adv_labels.npy".format(selection))

        all_train = np.concatenate((x_train,adv_samples),axis=0)
        all_labels = np.int64(np.concatenate((y_train,adv_labels),axis=0))
        print(">>>>>>>Train data:{},non_target_adv:{},target_adv:{}".format(all_train.shape,adv_samples.shape,x_train.shape))

        train_data = UserDataset(all_train,labels = all_labels,img_size = (32,32), transform=cifar10_transforms_train)
        test_data = UserDataset(x_test,labels = y_test,img_size = (32,32), transform=cifar10_transforms_test)
    elif dataset=="svhn_adv":
        x_train = np.load("../svhn/svhn_train_x.npy")
        y_train = np.int64(np.load("../svhn/svhn_train_y.npy").squeeze()-1)

        x_test = np.load("../svhn/svhn_test_x.npy")
        y_test = np.int64(np.load("../svhn/svhn_test_y.npy").squeeze()-1)
        
        
        adv_samples = np.uint8(np.load("../svhn/{}/PGD_adv_samples.npy".format(selection))*255)
        adv_labels = np.load("../svhn/{}/PGD_adv_labels.npy".format(selection))

        all_train = np.concatenate((x_train,adv_samples),axis=0)
        all_labels = np.int64(np.concatenate((y_train,adv_labels),axis=0))
        print(">>>>>>>Train data:{},adv:{}".format(all_train.shape,adv_samples.shape))

        train_data = UserDataset(all_train,labels = all_labels,img_size = (32,32), transform=svhn_transforms_train)
        test_data = UserDataset(x_test,labels = y_test,img_size = (32,32), transform=svhn_transforms_test)
    

    else:
        assert False,"undefined dataset:{}".format(dataset)
#     print("Load data successfully!",x_train.shape,y_train.max(),y_train.min())
    train_loader = DataLoader(train_data,batch_size=250, shuffle=True, num_workers=4, pin_memory=True)
    test_loader = DataLoader(test_data,batch_size=256, shuffle=False, num_workers=4, pin_memory=True)
    
    if oe_tuned:
        ood_path ="/home/lingjun/ood_2020/data/training_ood_data/tinyimagenet_cifar_40.npy"
        ood_sampels = np.load(ood_path)
        print(ood_sampels.shape)
        train_loader_out = make_dataloader(ood_sampels,None,img_size=(32,32), batch_size=100, transform_test=train_transform)
        train_loader_in = train_loader
    
    # Load model structure
    net,optimizer = select_model_optimizer(selection,opt,num_classes,dropRate)
    net.cuda()
    # Make save directory
    check_dir(save_path)
    with open(os.path.join(save_path,  '{}_training_results.csv'.format(model_name)), 'w') as f:
        f.write('epoch,time(s),train_loss,train_acc,test_loss,test_acc(%)\n')
    print('Beginning Training\n')
    # Main loop
    best_epoch = 0
    best_acc = 0.0
    for epoch in range(start_epoch, end_epoch):
        adjust_opt(opt, optimizer, epoch)
        if epoch % 50 == 0:
            print(">>>>LR:{}".format(optimizer.param_groups[0]['lr']))
        state['epoch'] = epoch
        begin_epoch = time.time()
        if oe_tuned:
            train_oe(net,train_loader_in,train_loader_out,optimizer,state)
        else:
            train_baseline(net,train_loader,optimizer,state)
        test(net,test_loader,state)
         # Save model
        if epoch==0:
            best_epoch = epoch
            best_acc = state['test_accuracy']
            cur_save_path = os.path.join(save_path, '{}_epoch_{}_{}.pt'.format(model_name,best_epoch,best_acc))
            # network.module.state_dict()
            if torch.cuda.device_count()>1:
                print(">>> use {} gpus".format(torch.cuda.device_count()))
                torch.save(net.module.state_dict(),cur_save_path)
            else:
                torch.save(net.state_dict(),cur_save_path)
        cur_acc = state['test_accuracy']
        if cur_acc > best_acc:
            cur_save_path = os.path.join(save_path, '{}_epoch_{}_{}.pt'.format(model_name,epoch,cur_acc))
            # network.module.state_dict()
            if torch.cuda.device_count()>1:
                torch.save(net.module.state_dict(),cur_save_path)
            else:
                torch.save(net.state_dict(),cur_save_path)
            prev_path = os.path.join(save_path, '{}_epoch_{}_{}.pt'.format(model_name,best_epoch,best_acc))
            if os.path.exists(prev_path): 
                os.remove(prev_path)
            best_epoch = epoch
            best_acc = cur_acc
        # Show results    
        with open(os.path.join(save_path,  '{}_training_results.csv'.format(model_name)), 'a') as f:
            f.write('%03d,%05d,%0.6f,%0.4f,%0.6f,%0.4f\n' % (
                (epoch + 1),
                time.time() - begin_epoch,
                state['train_loss'],
                state['train_accuracy'],
                state['test_loss'],
                state['test_accuracy'],
            ))
        print('|Epoch {0:3d} | Time {1:5d} | Train Loss {2:.4f}|  Train Acc {3:.4f} | Test Loss {4:.4f} | Test Acc {5:.4f}'.format(
        (epoch + 1),
        int(time.time() - begin_epoch),
        state['train_loss'],
        state['train_accuracy'],
        state['test_loss'],
        state['test_accuracy'])
        )
import sys
# CUDA_VISIBLE_DEVICES=2 python train_cifar.py resnet18
# CUDA_VISIBLE_DEVICES=0 python train_cifar.py densenet

# CUDA_VISIBLE_DEVICES=0 python train_cifar.py ConvnetCifar
# CUDA_VISIBLE_DEVICES=1 python train_cifar.py ConvnetCifarBN
# CUDA_VISIBLE_DEVICES=1 python train_cifar.py VGG16

if __name__=="__main__":

    selection = sys.argv[1]

    train_model(selection = selection,oe_tuned = False,dataset="cifar10",num_classes = 10)

    # train_model(selection = "densenet",oe_tuned = False,dataset="cifar10",num_classes = 10)


    
    
    
    # train_model(selection = "WRN_40_4",oe_tuned = True)
    