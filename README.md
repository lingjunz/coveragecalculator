This repository is used to caluculate neuron coverage for a given dataset. 

Before running the following command, you need to prepare a pre-trained model and the corresponding model structure. Besides that, you also should 
add specific configureations in `./utils/configs.py`. Currently, we support three different neuron coverage criteria, i.e. , nc_coverage, kmnc_coverage and nbc_coverage.

For details about neuron coverage, please reffer to the paper "DeepGauge Multi-Granularity Testing Criteria for Deep Learning Systems".

**example commands**   

* `CUDA_VISIBLE_DEVICES=1 python profileNN.py -net vgg16 -profile_output ./net_profiling -data_type cifar10 -coverage nbc_kmnc`

* `CUDA_VISIBLE_DEVICES=1 python profileNN.py -net ConvnetCifar -profile_output ./net_profiling -data_type cifar10 -coverage nbc_kmnc`

* `CUDA_VISIBLE_DEVICES=0 python profileNN.py -net ConvnetMnist -profile_output ./net_profiling -data_type mnist -coverage nbc_kmnc`
