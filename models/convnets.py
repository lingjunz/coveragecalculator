import torch.nn as nn 
import torch.nn.functional as F 
import torch.nn.init as init

import torch 

####
'''
 any model with or without the nn.BatchNorm2d will have big difference

'''
###


class ConvnetMnist(nn.Module):
    def __init__(self,num_classes=10):
        super(ConvnetMnist, self).__init__()
        self.conv1=nn.Conv2d(1, 64, kernel_size=3,padding=0)
        self.relu=nn.ReLU(inplace=True)
        self.conv2=nn.Conv2d(64, 64, kernel_size=3,padding=0)

        self.drop=  nn.Dropout()
        self.dens1 = nn.Linear(64 * 12 * 12, 128)
        self.dens2 = nn.Linear(128, num_classes)
    
    def feature_list(self,x):
        out_list = []
        name_list = [
            "conv1","conv1_act","conv2","conv2_act",
             "fc1","fc1_act",'logits']
        x=self.conv1(x)
        out_list.append(x)
        x=self.relu(x)
        out_list.append(x)
        x=self.conv2(x)
        out_list.append(x)
        x=self.relu(x)
        out_list.append(x)
        x1 = F.max_pool2d(x, 2, 2)
        
        x1_flatt=x1.view(x1.shape[0], -1)
        x1_flatt=self.drop(x1_flatt)
        
        x_out= self.dens1(x1_flatt)
        out_list.append(x_out)
        x_out= self.relu(x_out)
        out_list.append(x_out)
        logits= self.dens2(x_out)
        out_list.append(logits)
        return logits, out_list, name_list
        

    def forward(self, x,need_softmax=True):
        x=self.conv1(x)
        x=self.relu(x)
        x=self.conv2(x)
        x=self.relu(x)
        x1 = F.max_pool2d(x, 2, 2)
        
        x1_flatt=x1.view(x1.shape[0], -1)
        x1_flatt=self.drop(x1_flatt)
        
        x_out= self.dens1(x1_flatt)
        x_out= self.relu(x_out)
        x_out= self.dens2(x_out)
        if need_softmax:
            return F.log_softmax(x_out, dim=-1)
        return x_out 

class ConvnetCifar(nn.Module):
    def __init__(self,num_classes=10):
        super(ConvnetCifar, self).__init__()
        self.conv1=nn.Conv2d(3, 32, kernel_size=3,padding=1)
        self.relu=nn.ReLU(inplace=True)
        self.conv2=nn.Conv2d(32, 32, kernel_size=3,padding=1)

        self.conv3=nn.Conv2d(32, 64, kernel_size=3,padding=1)


        self.conv4=nn.Conv2d(64, 64, kernel_size=3,padding=1)

        self.conv5=nn.Conv2d(64, 128, kernel_size=3,padding=1)


        self.conv6=nn.Conv2d(128, 128, kernel_size=3,padding=1)


        self.drop=  nn.Dropout()

        self.dens1=nn.Linear(128 * 4 * 4, 1024)

        self.dens2=nn.Linear(1024, 512)

        self.dens3=nn.Linear(512, num_classes)
    
    def feature_list(self,x):
        out_list = []
        name_list = [
            "conv1","conv1_act","conv2","conv2_act",
            "conv3","conv3_act","conv4","conv4_act",
            "conv5","conv5_act","conv6","conv6_act",
             "fc1","fc1_act","fc2","fc2_act",'logits']
        
        x=self.conv1(x)
        out_list.append(x)
        x=self.relu(x)
        out_list.append(x)
        x=self.conv2(x)
        out_list.append(x)
        x=self.relu(x)
        out_list.append(x)
        x = F.max_pool2d(x, 2, 2)
        
        x=self.conv3(x)
        out_list.append(x)
        x=self.relu(x)
        out_list.append(x)
        x=self.conv4(x)
        out_list.append(x)
        x=self.relu(x)
        out_list.append(x)
        x = F.max_pool2d(x, 2, 2)
        
        
        x=self.conv5(x)
        out_list.append(x)
        x=self.relu(x)
        out_list.append(x)
        x=self.conv6(x)
        out_list.append(x)
        x=self.relu(x)
        out_list.append(x)
        x = F.max_pool2d(x, 2, 2)
        
        x1_flatt=x.view(x.shape[0], -1)
        
        x_out=self.drop(x1_flatt)
        x_out= self.dens1(x_out)
        out_list.append(x_out)
        x_out= self.relu(x_out)
        out_list.append(x_out)

        x_out=self.drop(x_out)
        x_out= self.dens2(x_out)
        out_list.append(x_out)
        x_out= self.relu(x_out)
        out_list.append(x_out)

        logits= self.dens3(x_out)
        out_list.append(logits)
        

        return logits, out_list, name_list 
        
    
    def forward(self, x,need_softmax=True):
        x=self.conv1(x)
        x=self.relu(x)
        x=self.conv2(x)
        x=self.relu(x)
        x = F.max_pool2d(x, 2, 2)
        
        x=self.conv3(x)
        x=self.relu(x)
        x=self.conv4(x)
        x=self.relu(x)
        x = F.max_pool2d(x, 2, 2)
        
        
        x=self.conv5(x)
        x=self.relu(x)
        x=self.conv6(x)
        x=self.relu(x)
        x = F.max_pool2d(x, 2, 2)
        
        x1_flatt=x.view(x.shape[0], -1)
        
        x_out=self.drop(x1_flatt)
        x_out= self.dens1(x_out)
        x_out= self.relu(x_out)

        x_out=self.drop(x_out)
        x_out= self.dens2(x_out)
        x_out= self.relu(x_out)

        x_out= self.dens3(x_out)
        
        if need_softmax:
            return F.log_softmax(x_out, dim=-1)
        return x_out 
    
    
    
    
    
class ConvnetMnistBN(nn.Module):
    def __init__(self,num_classes=10):
        super(ConvnetMnistBN, self).__init__()
        self.features=nn.Sequential(
             nn.Conv2d(1, 64, kernel_size=3,padding=0),
             nn.ReLU(inplace=True),
             nn.BatchNorm2d(64),
             nn.Conv2d(64, 64, kernel_size=3,padding=0),
             nn.ReLU(inplace=True),
          nn.BatchNorm2d(64),
             nn.MaxPool2d(2),
            )
        self.classifier = nn.Sequential(
            nn.Dropout(),
            nn.Linear(64 * 12 * 12, 128),
            nn.ReLU(inplace=True),
            nn.Linear(128, num_classes),
            )


    def forward(self, x):
        x1=self.features(x)
        
        x1_flatt=x1.view(x1.shape[0], -1)
        
        x_out= self.classifier(x1_flatt)
        #return F.log_softmax(x_out, dim=-1)
        return x_out 

class ConvnetCifarBN(nn.Module):
    def __init__(self,num_classes=10):
        super(ConvnetCifarBN, self).__init__()
        self.features=nn.Sequential(
             nn.Conv2d(3, 32, kernel_size=3,padding=1),
             nn.ReLU(inplace=True),
            nn.BatchNorm2d(32),
             nn.Conv2d(32, 32, kernel_size=3,padding=1),
             nn.ReLU(inplace=True),
            nn.BatchNorm2d(32),
             nn.MaxPool2d(2),
             nn.Conv2d(32, 64, kernel_size=3,padding=1),
             nn.ReLU(inplace=True),
            nn.BatchNorm2d(64),

             nn.Conv2d(64, 64, kernel_size=3,padding=1),
             nn.ReLU(inplace=True),
            nn.BatchNorm2d(64),

             nn.MaxPool2d(2),
             nn.Conv2d(64, 128, kernel_size=3,padding=1),
             nn.ReLU(inplace=True),
            nn.BatchNorm2d(128),

             nn.Conv2d(128, 128, kernel_size=3,padding=1),
             nn.ReLU(inplace=True),
            nn.BatchNorm2d(128),

             nn.MaxPool2d(2) )

        self.classifier  =nn.Sequential(
            nn.Dropout(),
            nn.Linear(128 * 4 * 4, 1024),
            nn.ReLU(inplace=True),
            nn.Dropout(),
            nn.Linear(1024, 512),
            nn.ReLU(inplace=True),
            nn.Linear(512, num_classes),
        )

    def forward(self, x):
        x1=self.features(x)
        
        x1_flatt=x1.view(x1.shape[0], -1)
        
        x_out= self.classifier(x1_flatt)
        #return F.log_softmax(x_out, dim=-1)
        return x_out 
    

