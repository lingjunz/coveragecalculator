import math

import torch
import torch.nn as nn
import torch.nn.functional as F


class Bottleneck(nn.Module):
    def __init__(self, in_planes, growth_rate, dropRate = 0.0):
        super(Bottleneck, self).__init__()
        self.bn1 = nn.BatchNorm2d(in_planes)
        self.conv1 = nn.Conv2d(in_planes, 4*growth_rate, kernel_size=1, bias=False)
        self.bn2 = nn.BatchNorm2d(4*growth_rate)
        self.conv2 = nn.Conv2d(4*growth_rate, growth_rate, kernel_size=3, padding=1, bias=False)
        self.droprate = dropRate
        
    def forward(self, x):
        out = self.conv1(F.relu(self.bn1(x)))
        if self.droprate > 0:
            out = F.dropout(out, p=self.droprate)
        out = self.conv2(F.relu(self.bn2(out)))
        if self.droprate > 0:
            out = F.dropout(out, p=self.droprate)
        out = torch.cat([out,x], 1)
        return out
    
    
    def extract_features(self, x):
        feature_list = []
        out = self.conv1(F.relu(self.bn1(x)))
        feature_list.append(out)
        if self.droprate > 0:
            out = F.dropout(out, p=self.droprate)
        out = self.conv2(F.relu(self.bn2(out)))
        feature_list.append(out)
        if self.droprate > 0:
            out = F.dropout(out, p=self.droprate)
        out = torch.cat([out,x], 1)
        return feature_list
        

class Transition(nn.Module):
    def __init__(self, in_planes, out_planes, dropRate = 0.0):
        super(Transition, self).__init__()
        self.bn = nn.BatchNorm2d(in_planes)
        self.conv = nn.Conv2d(in_planes, out_planes, kernel_size=1, bias=False)
        self.droprate = dropRate
        
    def forward(self, x):
        out = self.conv(F.relu(self.bn(x)))
        if self.droprate > 0:
            out = F.dropout(out, p=self.droprate)
        out = F.avg_pool2d(out, 2)
        return out
   
    def extract_features(self, x):
        feature_list = []
        out = self.conv(F.relu(self.bn(x)))
        feature_list.append(out)
        if self.droprate > 0:
            out = F.dropout(out, p=self.droprate)
        out = F.avg_pool2d(out, 2)
        return feature_list



class DenseNet(nn.Module):
    def __init__(self, block, nblocks, growth_rate=12, reduction=0.5, num_classes=10, dropRate = 0.0):
        super(DenseNet, self).__init__()
        self.growth_rate = growth_rate

        num_planes = 2*growth_rate
        self.conv1 = nn.Conv2d(3, num_planes, kernel_size=3, padding=1, bias=False)

        self.dense1 = self._make_dense_layers(block, num_planes, nblocks[0], dropRate)
        num_planes += nblocks[0]*growth_rate
        out_planes = int(math.floor(num_planes*reduction))
        self.trans1 = Transition(num_planes, out_planes, dropRate=dropRate)
        num_planes = out_planes

        self.dense2 = self._make_dense_layers(block, num_planes, nblocks[1], dropRate)
        num_planes += nblocks[1]*growth_rate
        out_planes = int(math.floor(num_planes*reduction))
        self.trans2 = Transition(num_planes, out_planes, dropRate=dropRate)
        num_planes = out_planes

        self.dense3 = self._make_dense_layers(block, num_planes, nblocks[2], dropRate)
        num_planes += nblocks[2]*growth_rate
        out_planes = int(math.floor(num_planes*reduction))
        self.trans3 = Transition(num_planes, out_planes, dropRate=dropRate)
        num_planes = out_planes

        self.dense4 = self._make_dense_layers(block, num_planes, nblocks[3], dropRate)
        num_planes += nblocks[3]*growth_rate

        self.bn = nn.BatchNorm2d(num_planes)
        self.linear = nn.Linear(num_planes, num_classes)
        
        self.block_num = len(nblocks)
        self.denses = {"dense1":self.dense1, "dense2":self.dense2, "dense3":self.dense3, "dense4":self.dense4}
        self.transes = {"trans1":self.trans1, "trans2":self.trans2, "trans3":self.trans3}
        

    def _make_dense_layers(self, block, in_planes, nblock, dropRate):
        layers = []
        for i in range(nblock):
            layers.append(block(in_planes, self.growth_rate, dropRate))
            in_planes += self.growth_rate
        return nn.Sequential(*layers)

    def forward(self, x):

        out = self.conv1(x)
        out = self.trans1.forward(self.dense1(out))
        out = self.trans2.forward(self.dense2(out))
        out = self.trans3.forward(self.dense3(out))
        out = self.dense4(out)
        out = F.avg_pool2d(F.relu(self.bn(out)), 4)
        out = out.view(out.size(0), -1)
        out = self.linear(out)
        return out
    
     # function to extact the multiple features
    def feature_list(self, x):
        feature_list = []
        name_list = []
        name_list.append("conv1")
        out = self.conv1(x)
        feature_list.append(out)
        for idx in range(self.block_num):
            cur_dense_idx = "dense{}".format(idx + 1)
            cur_dense_sequential = self.denses[cur_dense_idx]
            for i,(name,module) in enumerate(cur_dense_sequential._modules.items()):
                cur_features = module.extract_features(out)
                out = module(out)
                for j,each_features in enumerate(cur_features):
                    cur_name = "{}_{}_{}".format(cur_dense_idx,i,j)
                    name_list.append(cur_name)
                    feature_list.append(each_features)
            if idx<3: 
                cur_trans_idx = "trans{}".format(idx + 1)
                cur_trans_sequential = self.transes[cur_trans_idx]
                cur_features = cur_trans_sequential.extract_features(out)
                out = cur_trans_sequential(out)
                for j,each_features in enumerate(cur_features):
                    cur_name = "{}_{}".format(cur_trans_idx,j)
                    name_list.append(cur_name)
                    feature_list.append(each_features)
        # print("<<<",out.size())
        out = F.avg_pool2d(out, 4)
        out = out.view(out.size(0), -1) #  -1, self.in_planes)
        logits = self.linear(out)
        feature_list.append(logits)
        name_list.append("logits")
        return logits, feature_list, name_list

# def DenseNet121():
#     return DenseNet(Bottleneck, [6,12,24,16], growth_rate=32)

# def DenseNet169():
#     return DenseNet(Bottleneck, [6,12,32,32], growth_rate=32)

# def DenseNet201():
#     return DenseNet(Bottleneck, [6,12,48,32], growth_rate=32)

# def DenseNet161():
#     return DenseNet(Bottleneck, [6,12,36,24], growth_rate=48)

def densenet_cifar(num_classes=10,dropRate=0): #densenet121
    return DenseNet(Bottleneck, [6,12,24,16], growth_rate=12,num_classes=num_classes,dropRate=dropRate)

# ['conv1', 
# 'dense1_0_0', 'dense1_0_1', 'dense1_1_0', 'dense1_1_1', 'dense1_2_0', 'dense1_2_1', 'dense1_3_0', 'dense1_3_1', 'dense1_4_0', 'dense1_4_1', 'dense1_5_0', 'dense1_5_1', 
# 'trans1_0', 
# 'dense2_0_0', 'dense2_0_1', 'dense2_1_0', 'dense2_1_1', 'dense2_2_0', 'dense2_2_1', 'dense2_3_0', 'dense2_3_1', 'dense2_4_0', 'dense2_4_1', 'dense2_5_0', 'dense2_5_1', 'dense2_6_0', 'dense2_6_1', 'dense2_7_0', 'dense2_7_1', 'dense2_8_0', 'dense2_8_1', 'dense2_9_0', 'dense2_9_1', 'dense2_10_0', 'dense2_10_1', 'dense2_11_0', 'dense2_11_1', 
# 'trans2_0',
# 'dense3_0_0', 'dense3_0_1', 'dense3_1_0', 'dense3_1_1', 'dense3_2_0', 'dense3_2_1', 'dense3_3_0', 'dense3_3_1', 'dense3_4_0', 'dense3_4_1', 'dense3_5_0', 'dense3_5_1', 'dense3_6_0', 'dense3_6_1', 'dense3_7_0', 'dense3_7_1', 'dense3_8_0', 'dense3_8_1', 'dense3_9_0', 'dense3_9_1', 'dense3_10_0', 'dense3_10_1', 'dense3_11_0', 'dense3_11_1', 'dense3_12_0', 'dense3_12_1', 'dense3_13_0', 'dense3_13_1', 'dense3_14_0', 'dense3_14_1', 'dense3_15_0', 'dense3_15_1', 'dense3_16_0', 'dense3_16_1', 'dense3_17_0', 'dense3_17_1', 'dense3_18_0', 'dense3_18_1', 'dense3_19_0', 'dense3_19_1', 'dense3_20_0', 'dense3_20_1', 'dense3_21_0', 'dense3_21_1', 'dense3_22_0', 'dense3_22_1', 'dense3_23_0', 'dense3_23_1', 
# 'trans3_0', 
# 'dense4_0_0', 'dense4_0_1', 'dense4_1_0', 'dense4_1_1', 'dense4_2_0', 'dense4_2_1', 'dense4_3_0', 'dense4_3_1', 'dense4_4_0', 'dense4_4_1', 'dense4_5_0', 'dense4_5_1', 'dense4_6_0', 'dense4_6_1', 'dense4_7_0', 'dense4_7_1', 'dense4_8_0', 'dense4_8_1', 'dense4_9_0', 'dense4_9_1', 'dense4_10_0', 'dense4_10_1', 'dense4_11_0', 'dense4_11_1', 'dense4_12_0', 'dense4_12_1', 'dense4_13_0', 'dense4_13_1', 'dense4_14_0', 'dense4_14_1', 'dense4_15_0', 'dense4_15_1',
#  'logits']
