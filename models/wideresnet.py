import math
import torch
import torch.nn as nn
import torch.nn.functional as F

# __all__ = ['WideResNet']

class BasicBlock(nn.Module):
    def __init__(self, in_planes, out_planes, stride, dropRate=0.0):
        super(BasicBlock, self).__init__()
        self.bn1 = nn.BatchNorm2d(in_planes)
        self.relu1 = nn.ReLU(inplace=True)
        self.conv1 = nn.Conv2d(in_planes, out_planes, kernel_size=3, stride=stride,
                               padding=1, bias=False)
        self.bn2 = nn.BatchNorm2d(out_planes)
        self.relu2 = nn.ReLU(inplace=True)
        self.conv2 = nn.Conv2d(out_planes, out_planes, kernel_size=3, stride=1,
                               padding=1, bias=False)
        self.droprate = dropRate
        self.equalInOut = (in_planes == out_planes)
        self.convShortcut = (not self.equalInOut) and nn.Conv2d(in_planes, out_planes, kernel_size=1, stride=stride,
                               padding=0, bias=False) or None
    
    

    def forward(self, x):
        if not self.equalInOut:
            x = self.relu1(self.bn1(x))
        else:
            out = self.relu1(self.bn1(x))
        out = self.relu2(self.bn2(self.conv1(out if self.equalInOut else x)))
        if self.droprate > 0:
            out = F.dropout(out, p=self.droprate)
        out = self.conv2(out)
        return torch.add(x if self.equalInOut else self.convShortcut(x), out)

    def extract_features(self, x):
        feature_list = []
        if not self.equalInOut:
            x = self.relu1(self.bn1(x))
        else:
            out = self.relu1(self.bn1(x))
        out = out if self.equalInOut else x
        out = self.conv1(out)
        feature_list.append(out)
        out = self.relu2(self.bn2(out))
        if self.droprate > 0:
            out =  F.dropout(out, p=self.droprate, training=True)
        out = self.conv2(out)
        return feature_list
    

class NetworkBlock(nn.Module):
    def __init__(self, nb_layers, in_planes, out_planes, block, stride, dropRate=0.0):
        super(NetworkBlock, self).__init__()
        self.layer = self._make_layer(block, in_planes, out_planes, nb_layers, stride, dropRate)
    def _make_layer(self, block, in_planes, out_planes, nb_layers, stride, dropRate):
        layers = []
        for i in range(int(nb_layers)):
            layers.append(block(i == 0 and in_planes or out_planes, out_planes, i == 0 and stride or 1, dropRate))
        return nn.Sequential(*layers)
    def forward(self, x):
        return self.layer(x)

class WideResNet(nn.Module):
    def __init__(self, num_classes, depth=28, widen_factor=10, dropRate=0):
        super(WideResNet, self).__init__()
        nChannels = [16, 16*widen_factor, 32*widen_factor, 64*widen_factor]
        assert((depth - 4) % 6 == 0)
        n = (depth - 4) / 6
        block = BasicBlock
        # 1st conv before any network block
        self.conv1 = nn.Conv2d(3, nChannels[0], kernel_size=3, stride=1,
                               padding=1, bias=False)
        # 1st block
        self.block1 = NetworkBlock(n, nChannels[0], nChannels[1], block, 1, dropRate)
        # 2nd block
        self.block2 = NetworkBlock(n, nChannels[1], nChannels[2], block, 2, dropRate)
        # 3rd block
        self.block3 = NetworkBlock(n, nChannels[2], nChannels[3], block, 2, dropRate)
        # global average pooling and classifier
        self.bn1 = nn.BatchNorm2d(nChannels[3])
        self.relu = nn.ReLU(inplace=True)
        self.nChannels = nChannels[3]
        self.fc = nn.Linear(self.nChannels, num_classes)
        self.block_num = 3
        self.layers = {1:self.block1,2:self.block2,3:self.block3}
        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                n = m.kernel_size[0] * m.kernel_size[1] * m.out_channels
                m.weight.data.normal_(0, math.sqrt(2. / n))
            elif isinstance(m, nn.BatchNorm2d):
                m.weight.data.fill_(1)
                m.bias.data.zero_()
            elif isinstance(m, nn.Linear):
                m.bias.data.zero_()

    def forward(self, x):
        out = self.conv1(x)
        out = self.block1.forward(out)
        out = self.block2.forward(out)
        out = self.block3.forward(out)
        out = self.relu(self.bn1(out))
        out = F.avg_pool2d(out, 8)
        out = out.view(-1, self.nChannels)
        out = self.fc(out)
        return out
    
    #     function to extact the multiple features
    def feature_list(self, x):
        name_list = []
        feature_list = []
        name_list.append("conv1")
        out = self.conv1(x)
        feature_list.append(out)
        for idx in range(self.block_num):
            cur_idx = idx + 1
            cur_sequential = self.layers[cur_idx]
            for i,(name,module) in enumerate(cur_sequential._modules.items()):
                cur_features = module.extract_features(out)
                out = module(out)
                for j,each_features in enumerate(cur_features):
                    cur_name = "layer_{}_{}_{}".format(cur_idx,i,j)
                    name_list.append(cur_name)
                    feature_list.append(each_features)
        out = self.relu(self.bn1(out))
        out = F.avg_pool2d(out, 8)
        out = out.view(-1, self.nChannels)
        logits = self.fc(out)
        name_list.append("logits")
        feature_list.append(logits)
        return logits, feature_list, name_list
        
    
def WRN_28_10(num_classes=10, dropRate=0):
    return WideResNet(num_classes, depth=28, widen_factor=10, dropRate=0)

def WRN_40_4(num_classes=10, dropRate=0):
    return WideResNet(num_classes, depth=40, widen_factor=4, dropRate=0)

def WRN_16_4(num_classes=10, dropRate=0):
    return WideResNet(num_classes, depth=16, widen_factor=4, dropRate=0)

def WRN_16_8(num_classes=10, dropRate=0):
    return WideResNet(num_classes, depth=16, widen_factor=8, dropRate=0)