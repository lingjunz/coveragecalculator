
import numpy as np

concat = lambda x: np.concatenate(x,axis=0)
to_np = lambda x: x.data.cpu().numpy()

def profile_nc_coverage(net, dataloader, threshold, profile_dict, device,verbose=False):
    net.eval()
    profile_num = len(dataloader.dataset)
    coverage_rate = 0.0
    activated = []
    for i, (data,_) in enumerate(dataloader):
        data = data.to(device)
        _, out_list, name_list = net.feature_list(data)
        finished_num = i * dataloader.batch_size 
        if i == 0:
            neurons = [item.size(1) for item in out_list]
            total_neurons = np.sum(neurons)
            coverage_map = np.zeros(np.sum(neurons),dtype=np.uint8)
        if verbose:
            print("layer_name",name_list)
            print("neuron_num",neurons)
            print("total:",np.sum(neurons))
            for temp in range(len(out_list)):
                print(out_list[temp].shape)
            if i % 10 == 0:
                print("* finish {}/{} samples".format(finished_num,profile_num))
                print("* nc_coverage %.2f(%d/%d)"%(coverage_rate,np.sum(coverage_map),total_neurons))
        layer_start_index = 0
        for layer_id in range(len(out_list)):
            cur_layer = name_list[layer_id]
            cur_neurons_num = out_list[layer_id].size(1)
            if len(out_list[layer_id].shape)==4:
                neurons_value = to_np(out_list[layer_id]).mean(axis=(2,3)) # batch_num,cur_neurons
            else:
                neurons_value  = to_np(out_list[layer_id]) # batch_num,cur_neurons 
            for neuron_id in range(cur_neurons_num):
                cov_index = layer_start_index + neuron_id
                if coverage_map[cov_index]:
                    continue
                cur_neurons = neurons_value[:,neuron_id]
                profile_data_list = profile_dict[(cur_layer, neuron_id)]
                cur_lowerest = profile_data_list[-2]
                cur_highest = profile_data_list[-1]
                divider = cur_highest - cur_lowerest
                if divider == 0:
                    cur_neurons = np.zeros(cur_neurons.shape)
                else:
                    cur_neurons = (cur_neurons - cur_lowerest)/ divider 
                if np.sum(cur_neurons>threshold) > 0:
                    activated.append(cov_index)
                    coverage_map[cov_index] = 1
            layer_start_index += cur_neurons_num     
        coverage_rate = (100. * np.sum(coverage_map))/total_neurons
        
    if profile_num > 0 and verbose:
        print("* nc_coverage %.2f(%d/%d)"%(coverage_rate,np.sum(coverage_map),total_neurons))
    return coverage_rate