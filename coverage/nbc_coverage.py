
import numpy as np
concat = lambda x: np.concatenate(x,axis=0)
to_np = lambda x: x.data.cpu().numpy()

def profile_nbc_coverage(net, dataloader, profile_dict, device,nbc_sections=10, k=1000,verbose=False):
    assert k>0
    nbc_sections = (nbc_sections+1) * 2
    net.eval()
    profile_num = len(dataloader.dataset)
    coverage_rate = 0.0

    for i, (data,_) in enumerate(dataloader):
        data = data.to(device)
        _, out_list, name_list = net.feature_list(data)
        finished_num = i * dataloader.batch_size 
        if i == 0:
            neurons = [item.size(1) for item in out_list]
            total_neurons = np.sum(neurons)
            coverage_map = np.zeros(np.sum(neurons)*nbc_sections,dtype=np.uint8)
            print("layer_name",name_list)
            print("neuron_num",neurons)
            print("total:",np.sum(neurons))
            for temp in range(len(out_list)):
                print(out_list[temp].shape)
        if verbose:
            if i % 10 == 0 :
                print("* finish {}/{} samples".format(finished_num,profile_num))
                print("* nbc_coverage %.2f(%d/%d)"%(coverage_rate,np.sum(coverage_map),total_neurons*nbc_sections))
        
        layer_start_index = 0
        for layer_id in range(len(out_list)):
            cur_layer = name_list[layer_id]
            cur_neurons_num = out_list[layer_id].size(1)
            if len(out_list[layer_id].shape)==4:
                neurons_value = to_np(out_list[layer_id]).mean(axis=(2,3)) # batch_num,cur_neurons
            else:
                neurons_value  = to_np(out_list[layer_id]) # batch_num,cur_neurons 
            for single in neurons_value:
                for neuron_id in range(cur_neurons_num):
                    single_output = single[neuron_id]
                    profiling_data = profile_dict[(cur_layer,neuron_id)]
                    mean_value = profiling_data[0]
                    lower_bound = profiling_data[3]
                    upper_bound = profiling_data[4]
                    unit_range = (upper_bound - lower_bound) / k
                    if unit_range < 1e-4:
                        unit_range = 0.05
                    if single_output < lower_bound: # the hypo active case, the store targets from low to -infi
                        # float here
                        target_idx = (lower_bound - single_output) / unit_range
                    elif single_output > upper_bound:  # the hyperactive case
                        target_idx = (single_output - upper_bound) / unit_range
                    else:
                        continue

                    if target_idx > (nbc_sections - 1):
                        id = (layer_start_index + neuron_id )* nbc_sections + nbc_sections - 1
                    else:
                        id = (layer_start_index + neuron_id ) * nbc_sections + int(target_idx)
                    id = int(id)

                    if not coverage_map[id]:
                        coverage_map[id] = 1
            layer_start_index += cur_neurons_num
        coverage_rate = (100. * np.sum(coverage_map))/(total_neurons*nbc_sections)
    if profile_num > 0 and verbose:
        print("* nbc_coverage %.2f(%d/%d)"%(coverage_rate,np.sum(coverage_map),total_neurons*nbc_sections))
    return coverage_rate

    