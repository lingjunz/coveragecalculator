from torch.utils.data.dataset import Dataset
from PIL import Image
import numpy as np

class UserDataset(Dataset):

    def __init__(self, imgs ,img_size, labels = None, transform = None):
        self.transform = transform
        self.labels = labels
        self.total_num = imgs.shape[0]
        def load_image(idx):
            if img_size is not None:
                cur_img_arr = imgs[idx]
                img = Image.fromarray(np.uint8(cur_img_arr).squeeze())
                img = img.resize(img_size)
            else:
                img = imgs[idx]
            return img
        self.load_image = load_image
    
    def __getitem__(self,index):
        img = self.load_image(index)
        if self.transform is not None:
            img = self.transform(img)
        if self.labels is None:
            label = 0
        else:
            label = self.labels[index]
        return img, label  # 0 is the class
    
    def __len__(self):
        return self.total_num