import numpy as np
import torchvision.transforms as trn
import sys
sys.path.append("..")

from models.wideresnet import WRN_40_4
from models.densenet import densenet_cifar 
from models.resnet import ResNet18
from models.lenet import Lenet5,Lenet1
from models.vgg16 import VGG16
from models.convnets import ConvnetMnist,ConvnetCifar

concat = lambda x: np.concatenate(x,axis=0)
to_np = lambda x: x.data.cpu().numpy()

img_shape_dic = {
    "cifar10":(3,32,32),
    "mnist":(1,28,28)
}
img_size_dic = {
    "cifar10":(32,32),
    "mnist":(28,28)
}



class_num_dic = {
    'cifar10':10,
    "mnist":10,
    "cifar100":100,
    
}


cifar10_mean = [0.4914,0.4822,0.4465] #np.array([x / 255 for x in [125.3, 123.0, 113.9]])
cifar10_std = [0.2470,0.2435,0.2616]#np.array([x / 255 for x in [63.0, 62.1, 66.7]])

cifar100_mean = np.array([0.5071,0.4865,0.4409]) #50000 training samples
cifar100_std = np.array([0.2673,0.2564,0.2762])

svhn_mean = np.array([0.4377,0.4438,0.4728])
svhn_std = np.array([0.1980,0.2010,0.1970])

# imagenet
# means = [0.485, 0.456, 0.406] 
# stds = [0.229, 0.224, 0.225]

def cifar_preprocessing(x_test):
    temp = np.copy(x_test)
    temp = temp.astype('float32')
    mean = [0.4914,0.4822,0.4465]
    std = [0.2470,0.2435,0.2616]
    for i in range(3):
        temp[:, i, :, :] = (temp[:, i, :, :] - mean[i]) / std[i]
    return temp

def svhn_preprocessing(x_test):
    temp = np.copy(x_test)
    temp = temp.astype('float32')
    mean = [0.4377,0.4438,0.4728]
    std = [0.1980,0.2010,0.1970]
    for i in range(3):
        temp[:, i, :, :] = (temp[:, i, :, :] - mean[i]) / std[i]
    return temp

def mnist_preprocessing(x_test):
    return x_test

preprocess_func_dic = {
    "mnist":mnist_preprocessing,
    "cifar10":cifar_preprocessing,
    "svhn":svhn_preprocessing
}


cifar_transforms_train = trn.Compose([trn.RandomHorizontalFlip(), trn.RandomCrop(32, padding=2),trn.ToTensor(), trn.Normalize(cifar10_mean, cifar10_std)])
cifar_transforms_test = trn.Compose([trn.ToTensor(), trn.Normalize(cifar10_mean, cifar10_std)])

cifar100_transforms_train = trn.Compose([trn.RandomHorizontalFlip(), trn.RandomCrop(32, padding=2),trn.ToTensor(), trn.Normalize(cifar100_mean, cifar100_std)])
cifar100_transforms_test = trn.Compose([trn.ToTensor(), trn.Normalize(cifar100_mean, cifar100_std)])

svhn_transforms_train = trn.Compose([trn.RandomHorizontalFlip(), trn.RandomCrop(32, padding=2),trn.ToTensor(), trn.Normalize(svhn_mean, svhn_std)])
svhn_transforms_test = trn.Compose([trn.ToTensor(), trn.Normalize(svhn_mean, svhn_std)])

minst_transform = trn.Compose([trn.ToTensor()])

transforms_dic = {
    "mnist":minst_transform,
    "cifar10_train":cifar_transforms_train,
    "cifar10_test":cifar_transforms_test,
    "cifar100_train":cifar100_transforms_train,
    "cifar100_test":cifar100_transforms_test,
    "svhn_train":svhn_transforms_train,
    "svhn_test":svhn_transforms_test,
}


model_dic = {
   
    "mnist":{
        "ConvnetMnist":ConvnetMnist(),
#         "lenet5":Lenet5( num_classes=10),
#         "lenet5_dropout":Lenet5( num_classes=10,dropRate=0.25),
#         "lenet5_oe":Lenet5( num_classes=10),
#         "lenet5_oe_dropout":Lenet5( num_classes=10,dropRate=0.25),
        
#         "lenet1":Lenet1( num_classes=10),
#         "lenet1_dropout":Lenet1( num_classes=10,dropRate=0.25),
#         "lenet1_oe":Lenet1( num_classes=10),
#         "lenet1_oe_dropout":Lenet1( num_classes=10,dropRate=0.25),
    },
#     "fmnist":{
#         "lenet5":Lenet5( num_classes=10),
#         "lenet5_dropout":Lenet5( num_classes=10,dropRate=0.25),
#         "lenet5_oe":Lenet5( num_classes=10),
#         "lenet5_oe_dropout":Lenet5( num_classes=10,dropRate=0.25),
        
#         "lenet1":Lenet1( num_classes=10),
#         "lenet1_dropout":Lenet1( num_classes=10,dropRate=0.25),
#         "lenet1_oe":Lenet1( num_classes=10),
#         "lenet1_oe_dropout":Lenet1( num_classes=10,dropRate=0.25),
#     },
    "cifar10":{
        'vgg16':VGG16(),
        'ConvnetCifar':ConvnetCifar(),
        
        
#         'resnet18':ResNet18(num_classes=10, dropRate=0),
#         'resnet18_oe':ResNet18(num_classes=10, dropRate=0),
#         'resnet18_dropout':ResNet18(num_classes=10,dropRate=0.25),
#         'resnet18_oe_dropout':ResNet18(num_classes=10,dropRate=0.25),
        
#         "densenet":densenet_cifar(num_classes=10,dropRate=0),#(depth=40, num_classes=10),
#         "densenet_oe":densenet_cifar(num_classes=10,dropRate=0),#(depth=40, num_classes=10),
#         "densenet_dropout":densenet_cifar(num_classes=10,dropRate=0.25),#(depth=40, num_classes=10),
#         "densenet_oe_dropout":densenet_cifar(num_classes=10,dropRate=0.25),#(depth=40, num_classes=10),
    },
#     "cifar100":{
#         'resnet18':ResNet18(num_classes=100, dropRate=0),
#         'resnet18_oe':ResNet18(num_classes=100, dropRate=0),
#         'resnet18_dropout':ResNet18(num_classes=100,dropRate=0.25),
#         'resnet18_oe_dropout':ResNet18(num_classes=100,dropRate=0.25),
        
#         "densenet":densenet_cifar(num_classes=100,dropRate=0),#(depth=40, num_classes=10),
#         "densenet_oe":densenet_cifar(num_classes=100,dropRate=0),#(depth=40, num_classes=10),
#         "densenet_dropout":densenet_cifar(num_classes=100,dropRate=0.25),#(depth=40, num_classes=10),
#         "densenet_oe_dropout":densenet_cifar(num_classes=100,dropRate=0.25),#(depth=40, num_classes=10),
#     }

}

weight_path_dic = {

    "mnist":{
        "ConvnetMnist":"/home/lingjun/ood_2020/CoverageCalculator/pretrained/mnist_pytorch_ConvnetMnist_100/ConvnetMnist_baseline_epoch_97_0.9947.pt",

#         "lenet1":"/home/lingjun/ood_2020/ood_detection/train_models/MNIST_pytorch_lenet1_100/lenet1_baseline_epoch_60_0.9897.pt",
#         "lenet1_dropout":"/home/lingjun/ood_2020/ood_detection/train_models/MNIST_pytorch_lenet1_100/lenet1_baseline_epoch_60_0.9897.pt",
#         "lenet1_oe":"/home/lingjun/ood_2020/ood_detection/train_models/MNIST_pytorch_lenet1_100/lenet1_oe_epoch_60_0.9827.pt",
#         "lenet1_oe_dropout":"/home/lingjun/ood_2020/ood_detection/train_models/MNIST_pytorch_lenet1_100/lenet1_oe_epoch_60_0.9827.pt",

#         "lenet5":"/home/lingjun/ood_2020/ood_detection/train_models/MNIST_pytorch_lenet5_100/lenet5_baseline_epoch_71_0.9924.pt",
#         "lenet5_dropout":"/home/lingjun/ood_2020/ood_detection/train_models/MNIST_pytorch_lenet5_100/lenet5_baseline_epoch_71_0.9924.pt",
#         "lenet5_oe":"/home/lingjun/ood_2020/ood_detection/train_models/MNIST_pytorch_lenet5_100/lenet5_oe_epoch_70_0.9909.pt",
#         "lenet5_oe_dropout":"/home/lingjun/ood_2020/ood_detection/train_models/MNIST_pytorch_lenet5_100/lenet5_oe_epoch_70_0.9909.pt",
    },

#     "fmnist":{
#         "lenet1":"/home/lingjun/ood_2020/ood_detection/train_models/FMNIST_pytorch_lenet1_200/lenet1_baseline_epoch_193_0.8862.pt",
#         "lenet1_dropout":"/home/lingjun/ood_2020/ood_detection/train_models/FMNIST_pytorch_lenet1_200/lenet1_baseline_epoch_193_0.8862.pt",
#         "lenet1_oe":"/home/lingjun/ood_2020/ood_detection/train_models/FMNIST_pytorch_lenet1_200/lenet1_oe_epoch_199_0.837.pt",
#         "lenet1_oe_dropout":"/home/lingjun/ood_2020/ood_detection/train_models/FMNIST_pytorch_lenet1_200/lenet1_oe_epoch_199_0.837.pt",

#         "lenet5":"/home/lingjun/ood_2020/ood_detection/train_models/FMNIST_pytorch_lenet5_200/lenet5_baseline_epoch_105_0.9005.pt",
#         "lenet5_dropout":"/home/lingjun/ood_2020/ood_detection/train_models/FMNIST_pytorch_lenet5_200/lenet5_baseline_epoch_105_0.9005.pt",
#         "lenet5_oe":"/home/lingjun/ood_2020/ood_detection/train_models/FMNIST_pytorch_lenet5_200/lenet5_oe_epoch_114_0.8959.pt",
#         "lenet5_oe_dropout":"/home/lingjun/ood_2020/ood_detection/train_models/FMNIST_pytorch_lenet5_200/lenet5_oe_epoch_114_0.8959.pt",
#     },
    "cifar10":{
        'ConvnetCifar':"/home/lingjun/ood_2020/CoverageCalculator/pretrained/cifar10_pytorch_ConvnetCifar_200/ConvnetCifar_baseline_epoch_195_0.8464.pt",
        'vgg16':"/home/lingjun/ood_2020/DeepEvolve_torch/pretrained/cifar10_pytorch_VGG16_200/VGG16_baseline_epoch_181_0.9175.pt",
        
#         "resnet18":"/home/lingjun/ood_2020/ood_detection/train_models/CIFAR10_pytorch_resnet18_200/resnet18_baseline_epoch_175_0.9355.pt",
#         "resnet18_dropout":"/home/lingjun/ood_2020/ood_detection/train_models/CIFAR10_pytorch_resnet18_200/resnet18_baseline_epoch_175_0.9355.pt",
#         "resnet18_oe":"/home/lingjun/ood_2020/ood_detection/train_models/CIFAR10_pytorch_resnet18_200/resnet18_oe_epoch_160_0.9377.pt",
#         "resnet18_oe_dropout":"/home/lingjun/ood_2020/ood_detection/train_models/CIFAR10_pytorch_resnet18_200/resnet18_oe_epoch_160_0.9377.pt",
        
#         "densenet":"/home/lingjun/ood_2020/ood_detection/train_models/CIFAR10_pytorch_densenet_200/densenet_baseline_epoch_130_0.938.pt",
#         "densenet_dropout":"/home/lingjun/ood_2020/ood_detection/train_models/CIFAR10_pytorch_densenet_200/densenet_baseline_epoch_130_0.938.pt",
#         "densenet_oe":"/home/lingjun/ood_2020/ood_detection/train_models/CIFAR10_pytorch_densenet_200/densenet_oe_epoch_195_0.9379.pt",
#         "densenet_oe_dropout":"/home/lingjun/ood_2020/ood_detection/train_models/CIFAR10_pytorch_densenet_200/densenet_oe_epoch_195_0.9379.pt"
    },
#     "cifar100":{
#         "resnet18":"/home/lingjun/ood_2020/ood_detection/train_models/CIFAR100_pytorch_resnet18_200/resnet18_baseline_epoch_126_0.7039.pt",
#         "resnet18_dropout":"/home/lingjun/ood_2020/ood_detection/train_models/CIFAR100_pytorch_resnet18_200/resnet18_baseline_epoch_126_0.7039.pt",
#         "resnet18_oe":"/home/lingjun/ood_2020/ood_detection/train_models/CIFAR100_pytorch_resnet18_200/resnet18_oe_epoch_124_0.7048.pt",
#         "resnet18_oe_dropout":"/home/lingjun/ood_2020/ood_detection/train_models/CIFAR100_pytorch_resnet18_200/resnet18_oe_epoch_124_0.7048.pt",

#         "densenet":"/home/lingjun/ood_2020/ood_detection/train_models/CIFAR100_pytorch_densenet_200/densenet_baseline_epoch_195_0.7319.pt",
#         "densenet_dropout":"/home/lingjun/ood_2020/ood_detection/train_models/CIFAR100_pytorch_densenet_200/densenet_baseline_epoch_195_0.7319.pt",
#         "densenet_oe":"/home/lingjun/ood_2020/ood_detection/train_models/CIFAR100_pytorch_densenet_200/densenet_oe_epoch_123_0.7393.pt",
#         "densenet_oe_dropout":"/home/lingjun/ood_2020/ood_detection/train_models/CIFAR100_pytorch_densenet_200/densenet_oe_epoch_123_0.7393.pt"
#     }
    
   
}