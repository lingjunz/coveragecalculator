
import torch
import numpy as np
from scipy.stats import entropy

from .MyDataset import UserDataset
from .configs import model_dic,weight_path_dic,class_num_dic
import sys
sys.path.append("../")
from .cal_acc import cal_accuracy


concat = lambda x: np.concatenate(x,axis=0)
to_np = lambda x: x.data.cpu().numpy()


def make_dataloader(samples, labels, img_size=(32,32), batch_size=256, transform_test=None, shuffle=False):
    ood_dataset = UserDataset(samples,labels = labels,img_size = img_size, transform=transform_test)
    ood_loader = torch.utils.data.DataLoader(ood_dataset,batch_size=batch_size, shuffle=shuffle, num_workers=4, pin_memory=True)
    return ood_loader

def load_net(data_type,model_name,  device=torch.device('cuda:0')):
    net = model_dic[data_type][model_name]
    weight_path = weight_path_dic[data_type][model_name]
    net.to(device)#cuda()
    net.load_state_dict(torch.load(weight_path))
    print("Load {} model successfully!\nweight_path:{}".format(model_name,weight_path))
    return net


def load_torch_data(data_type, net, transform_test, from_train = True, sample_num=0):
    from keras.datasets import cifar10,mnist
    num_classes = class_num_dic[data_type]
    if data_type == "mnist":
        (x_train,y_train),(x_test,y_test) = mnist.load_data()
        img_size = (28,28)
        batch_size = 256
    elif data_type == "cifar10":
        (x_train,y_train),(x_test,y_test) = cifar10.load_data()
        img_size = (32,32)
        batch_size = 256
    else:
        assert False, "undefined data type!"    
    x = x_train if from_train else x_test
    y = y_train if from_train else y_test
    y = y.squeeze()
    dataloader = make_dataloader(x,y,
                                img_size=img_size, 
                                batch_size=batch_size, 
                                transform_test=transform_test,
                                shuffle=False)
    _,preds,_ = cal_accuracy(net,dataloader,info=False)
    correct_indexes = np.where(preds==y)[0]
    x,y = x[correct_indexes],y[correct_indexes]
    acc = np.round(len(correct_indexes)/len(preds),4)
    print("correct:{},({}/{})".format(len(correct_indexes),len(preds),acc))
    if sample_num <= 0:
        return x,y
    else:
        chosen_indexes = []
        for i in range(num_classes):
            cur_indexes = np.random.choice(np.where(y==i)[0],sample_num,replace=False)
            chosen_indexes += list(cur_indexes)
        chosen_indexes = np.array(chosen_indexes)
        np.random.shuffle(chosen_indexes)
        return x[chosen_indexes],y[chosen_indexes]
    
# def get_output(net,test_loader,layer_index = 3):
#     net.eval()
#     output = []
#     penultimate = []
#     with torch.no_grad():
#         for data,target in test_loader:
#             target = target.long()
#             data,target = data.cuda(),target.cuda()
#             # forward
#             _output,_penultimate = net.feature_list(data)
#             output.append(to_np(_output))
#             penultimate.append(to_np(_penultimate[layer_index]))
#     return concat(output), concat(penultimate)



def block_split(X, Y, train_num, partition = 5000):

    X_pos, Y_pos = X[:partition], Y[:partition]
    X_neg, Y_neg = X[partition:], Y[partition:]
    np.random.seed(0)
    random_index = np.arange(partition)
    np.random.shuffle(random_index)
    X_pos, Y_pos = X_pos[random_index], Y_pos[random_index]
    X_neg, Y_neg = X_neg[random_index], Y_neg[random_index]

    X_train = np.concatenate((X_pos[:train_num], X_neg[:train_num]))
    Y_train = np.concatenate((Y_pos[:train_num], Y_neg[:train_num]))
    X_test = np.concatenate((X_pos[train_num:], X_neg[train_num:]))
    Y_test = np.concatenate((Y_pos[train_num:], Y_neg[train_num:]))

    return X_train, Y_train, X_test, Y_test

def merge_and_generate_labels(X_pos, X_neg):

    X = np.concatenate((X_pos, X_neg))
    if len(X.shape)==1:
        X = X.reshape((X.shape[0], -1))
    y = np.concatenate((np.ones(X_pos.shape[0]), np.zeros(X_neg.shape[0])))
    return X,y


def make_mixed_dataloader(id_samples,ood_samples, img_size=(32,32), batch_size=256, transform_test=None):

    num = len(ood_samples)+len(id_samples)
    ood_labels = np.ones(len(ood_samples))
    id_labels = np.zeros(len(id_samples))
    all_samples = np.concatenate((ood_samples,id_samples),axis=0)
    all_labels = np.concatenate((ood_labels,id_labels),axis=0)
    indexes = np.arange(num)
    np.random.shuffle(indexes)
    all_samples,all_labels = all_samples[indexes],all_labels[indexes]   
    mix_dataloader = make_dataloader(all_samples,all_labels,img_size,batch_size,transform_test)
    return mix_dataloader,all_labels

# def calculate_certainty(net,dataloader,orig_preds,repeat_num = 50):
#     counter = np.zeros(len(dataloader.dataset))
#     for i in range(repeat_num):
#         _,cur_preds,_ = cal_accuracy(net,dataloader)
#         counter += cur_preds== orig_preds
#     certainty = counter/repeat_num
#     print("Finish calculations on certainty!")
#     return certainty

def calculate_certainty(net, dataloader, orig_preds, orig_vectors, repeat_num = 50):
    counter = np.zeros(len(dataloader.dataset)) # sample_num
    PCS = np.zeros((len(dataloader.dataset),repeat_num)) # sample_num * repeat_num
    PE = np.zeros(len(dataloader.dataset)) # sample
    mean_pred_vector = None
    all_pred_vectors = []
    for i in range(repeat_num):
        _,cur_preds,pred_vectors = cal_accuracy(net,dataloader,False)
        if i==0:
            mean_pred_vector = pred_vectors # sample_num * class_num
        else:
            mean_pred_vector += pred_vectors
        all_pred_vectors.append(pred_vectors)
        pred_vectors = np.sort(pred_vectors,axis=1)
        PCS[:,i] = pred_vectors[:,-1]-pred_vectors[:,-2]
        counter += cur_preds == orig_preds
    mean_pred_vector = mean_pred_vector/repeat_num
    PE = entropy(mean_pred_vector,axis=1) # sample_num
    VRO = 1-counter/repeat_num   # sample_num
    PCS = np.mean(PCS,axis=1)    # sample_num
    print("Finish calculations on certainty!")
    return PE,VRO,PCS,all_pred_vectors


